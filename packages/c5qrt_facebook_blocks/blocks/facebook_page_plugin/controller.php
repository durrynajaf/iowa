<?php
namespace Concrete\Package\C5qrtFacebookBlocks\Block\FacebookPagePlugin;

use Concrete\Core\Block\BlockController;

defined('C5_EXECUTE') or die("Access Denied.");

class Controller extends BlockController
{

    /**
     * Variables
     *
     * @var type 
     */
    protected $btTable = "btFacebookPagePlugin";
    protected $btInterfaceWidth = "650";
    protected $btInterfaceHeight = "400";
    protected $btDefaultSet = 'social';

    /**
     * Function to get block type.
     * 
     * @return type
     */
    public function getBlockTypeName()
    {
        return t('QRT - Facebook Page Plugin');
    }

    /**
     * Function to get Block Description.
     * 
     * @return type
     */
    public function getBlockTypeDescription()
    {
        return t("Facebook Page Plugin - To display different pages tabs like timeline etc. on your website.");
    }

    /**
     * Function to validate input
     * 
     * @return type
     */
    public function validate($args)
    {

        $e = $this->app->make("helper/validation/error");
        if (empty($args['FacebookURL'])) {
            $e->add(t('"Facebook Page URL" is a required field'));
        }
        else if (filter_var($args['FacebookURL'], FILTER_VALIDATE_URL) === false || strpos($args['FacebookURL'], "facebook.com/") === false) {
            $e->add(t('"Facebook Page URL" is invalid. Valid example: https://www.facebook.com/FacebookIndia'));
        }
        return $e;
    }


    /**
     * Function to save input
     * 
     * @return type
     */
    public function save($args)
    {
        !$args['Height'] ? $args['Height'] = 0 : '';
        !$args['Width'] ? $args['Width'] = 0 : '';
        parent::save($args);
    }

     /**
     * Function to validate input
     * 
     * @return type
     */
    public function __construct($obj = null)
    {
        parent::__construct($obj);
        if (!is_object($this->app)) {
            $this->app = \Concrete\Core\Support\Facade\Application::getFacadeApplication();
        }
        $this->set('app', $this->app);
    }

}
