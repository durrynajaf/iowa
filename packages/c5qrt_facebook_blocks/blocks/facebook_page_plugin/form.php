<?php
defined('C5_EXECUTE') or die("Access Denied.");
print $app->make('helper/concrete/ui')->tabs(array(
    array('tab1', t('Configuration'), true),
    array('tab2', t('Preview'))
));
?>
    <div id="ccm-tab-content-tab1" class="ccm-tab-content">
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group clearfix">
                    <?php echo $form->label('FacebookURL', t('Facebook Page URL:'), ['class' => 'required']); ?>
                    <?php echo $form->text('FacebookURL', $FacebookURL, ['placeholder' => t('The URL of the Facebook Page.')]); ?>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group clearfix">
                    <?php echo $form->label('FacebookPageTabs', t('Tabs (e.g., timeline, messages, events):')); ?>
                    <?php echo $form->text('FacebookPageTabs', $FacebookPageTabs, ['placeholder' => t('Tabs (e.g., timeline, messages, events)')]); ?>
                    <span class="label label-info">Default value: timeline</label>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group clearfix">
                    <?php echo $form->label('FlagUseSmallHeader', t('Use Small Header:')); ?>
                    <?php echo $form->select('FlagUseSmallHeader', [0 => 'No', 1 => 'Yes'], $FlagUseSmallHeader); ?>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group clearfix">
                    <?php echo $form->label('FlagHideCoverPhoto', t('Hide Cover Photo:')); ?>
                    <?php echo $form->select('FlagHideCoverPhoto', [0 => 'No', 1 => 'Yes'], $FlagHideCoverPhoto); ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group clearfix">
                    <?php echo $form->label('FlagApdaptContainer', t('Adapt to plugin container width:')); ?>
                    <?php echo $form->select('FlagApdaptContainer', [0 => 'No', 1 => 'Yes'], $FlagApdaptContainer, ['onchange' => 'chnApdat()']); ?>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group clearfix">
                    <?php echo $form->label('FlagShowFriendFaces', t('Show Friend\'s Faces:')); ?>
                    <?php echo $form->select('FlagShowFriendFaces', [0 => 'No', 1 => 'Yes'], $FlagShowFriendFaces); ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group clearfix">
                    <?php echo $form->label('Height', t('Height:')); ?>
                    <?php echo $form->number('Height', $Height, ['placeholder' => t('The pixel height(Min. 70)')]); ?>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group clearfix width-div">
                    <?php echo $form->label('Width', t('Width:')); ?>
                    <?php echo $form->number('Width', $Width, ['placeholder' => t('The pixel width(Min. 180 to Max. 500)')]); ?>
                </div>
            </div>
        </div>
    </div>
    <div id="ccm-tab-content-tab2" class="ccm-tab-content">
        <a class="btn btn-primary mrg-btm" onclick="updateIframe(false)"><i class="fa fa-refresh"></i> <?php echo t('Refresh Preview'); ?></a>
        <div><small><em><?php echo t('If you are seeing a blank preview that means Facebook does not recognize your Page URL. Please check the URL.'); ?></em></small></div>
        <iframe id="iframe-fb" src="" style="border:none;overflow:hidden; width: 100%;" allow="encrypted-media"></iframe>
    </div>

<script>
    function updateIframe(load) {
        if ($('#FacebookURL').val() != '') {
            url = 'https://www.facebook.com/plugins/page.php?href=&tabs=timeline&width=&height=&small_header=&adapt_container_width=&hide_cover=&show_facepile=';
            url = updateURL(url, 'href', $('#FacebookURL').val());
            
            if ($('#FacebookPageTabs').val() != '') {
                url = updateURL(url, 'tabs', $('#FacebookPageTabs').val());
            }
            var adapt = getBoolean($('#FlagApdaptContainer').val());
            var width = $('#Width').val();
            var height = $('#Height').val();
            width = width > 0 ? width : '180';
            height = height > 0 ? height : '70';
            if (!adapt) {
                url = updateURL(url, 'width', width);
            } 
            url = updateURL(url, 'height', height);
            url = updateURL(url, 'small_header', getBoolean($('#FlagUseSmallHeader').val()));
            url = updateURL(url, 'adapt_container_width', getBoolean($('#FlagApdaptContainer').val()));
            url = updateURL(url, 'hide_cover', getBoolean($('#FlagHideCoverPhoto').val()));
            url = updateURL(url, 'show_facepile', getBoolean($('#FlagShowFriendFaces').val()));
            document.getElementById('iframe-fb').src = url;
            if (!adapt) {
                document.getElementById('iframe-fb').width  = width;
            } else {
                document.getElementById('iframe-fb').width  = '100%';
            }
            document.getElementById('iframe-fb').height = height;
        } else if(!load) {
            alert('Facebook page URL is required for the preview.');
        }
    }

    function getBoolean(value){
        switch(value){
            case true:
            case "true":
            case 1:
            case "1":
            case "on":
            case "yes":
                return true;
            default: 
                return false;
        }
    }

    function updateURL(uri, key, value) {
        var re = new RegExp("([?&])" + key + "=.*?(&|#|$)", "i");
        if( value === undefined ) {
            if (uri.match(re)) {
                return uri.replace(re, '$1$2');
            } else {
                return uri;
            }
        } else {
            if (uri.match(re)) {
                return uri.replace(re, '$1' + key + "=" + value + '$2');
            } else {
                var hash =  '';
                if( uri.indexOf('#') !== -1 ){
                    hash = uri.replace(/.*#/, '#');
                    uri = uri.replace(/#.*/, '');
                }
                var separator = uri.indexOf('?') !== -1 ? "&" : "?";    
                return uri + separator + key + "=" + value + hash;
            }
        }  
    }

    function chnApdat()
    {
        var val = $('#FlagApdaptContainer').val();
        if (val!=1) {
            $('.width-div').show();
        } else {
            $('.width-div').hide();
        }
    }

    $(document).ready(function(){
        updateIframe(true);
        chnApdat();
    });
</script>
<style>
   #ccm-tab-content-tab1 .required:after {content: "*"; color: red;}
   #ccm-tab-content-tab2 .mrg-btm { margin-bottom: 10px !important; }
</style>