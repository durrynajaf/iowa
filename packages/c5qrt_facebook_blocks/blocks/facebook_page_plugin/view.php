<?php
defined('C5_EXECUTE') or die("Access Denied.");

?>
<iframe class="embed-responsive-item" src="https://www.facebook.com/plugins/page.php?href=<?php echo $FacebookURL; ?>&tabs=<?php echo $FacebookPageTabs ? $FacebookPageTabs : 'timeline'; ?><?php echo $FlagApdaptContainer == 1 ? '' : '&width='.$Width; ?>&height=<?php echo $Height; ?>&small_header=<?php echo $FlagUseSmallHeader == 1 ? 'true' : 'false'; ?>&adapt_container_width=<?php echo $FlagApdaptContainer == 1 ? 'true' : 'false'; ?>&hide_cover=<?php echo $FlagHideCoverPhoto == 1 ? 'true' : 'false'; ?>&show_facepile=<?php echo $FlagShowFriendFaces == 1 ? 'true' : 'false'; ?>" <?php echo $Width > 0 && $FlagApdaptContainer != 1 ? 'width="' . $Width . '"' : ''; ?> <?php echo $Height > 0 ? 'height="' . $Height . '"' : ''; ?> style="border:none;overflow:hidden; <?php echo $FlagApdaptContainer == 1 ? 'width:100%;' :''; ?>" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>
