<?php
namespace Concrete\Package\C5qrtFacebookBlocks;

defined('C5_EXECUTE') or die("Access Denied.");

use Package;
use SinglePage;
use BlockType;

/**
 * Main controller of the package
 */
class Controller extends Package
{
    /**
     * Protected variables 
     * 
     * @var string
     */
    protected $pkgHandle = 'c5qrt_facebook_blocks';
    protected $appVersionRequired = '5.7.5.13';
    protected $pkgVersion = '0.0.3';


    /**
     * Function get package name
     * 
     * @return string
     * @author JS 05/16/2017
     */
    public function getPackageName()
    {
        return t("QRT Facebook Blocks");
    }

    /**
     * Function to get the description of the package
     * 
     * @return string 
     * @author JS 05/16/2017
     */
    public function getPackageDescription()
    {
        return t("Facebook Blocks developed by C5QRT.");
    }
    
     /**
     * Function to overwrite the core install function of the package
     * 
     * @author JS 05/16/2017
     */
    public function install()
    {
        $pkg = parent::install();
        $this->install_blocks($pkg);
    }
    
    /**
     * Function to install blocks
     * 
     * @param type $pkg
     */
    function install_blocks($pkg) {
        BlockType::installBlockType('facebook_page_plugin', $pkg);
    }
        
   
}

