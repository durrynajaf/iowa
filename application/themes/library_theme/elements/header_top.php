<?php     defined('C5_EXECUTE') or die(_("Access Denied.")); ?>
<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<!-- the "no-js" class is for Modernizr. -->
<head>

<?php  View::element('header_required'); // makes Concrete5 edit bar show up ?> 
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="<?=$view->getThemePath()?>/webfonts/css/all.css" type="text/css"  />
  <link rel="stylesheet" href="<?=$view->getThemePath()?>/css/fontawesome.css" type="text/css"  />
  <link rel="stylesheet" href="<?=$view->getThemePath()?>/style.css" type="text/css"  />    
<link href="https://fonts.googleapis.com/css?family=Open+Sans|Roboto+Slab:400,700&display=swap" rel="stylesheet">

<!-- all our JS is at the bottom of the page, except for Modernizr. -->
<script src="//cdnjs.cloudflare.com/ajax/libs/modernizr/2.6.2/modernizr.min.js"></script>

<?php

//if( $_SERVER['HTTP_HOST'] == 'library2.pixoinc.com') {
$page = Page::getCurrentPage();
$app = \Concrete\Core\Support\Facade\Application::getFacadeApplication();
$service = $app->make('site');
$site = $service->getSite();
$siteController = $service->getController($site);
/*echo $site->getSiteID()."<br>";
echo $site->getSiteTreeID()."<br>";
echo $site->getSiteHomePageID();*/
$page = Page::getByID($site->getSiteHomePageID());
/*print_r($page);*/
$siteColor = $page->getAttribute('site_color');
$h1_color = $page->getAttribute('h1_color');
$h2_color = $page->getAttribute('h2_color');
$h3_color = $page->getAttribute('h3_color');
$search_field_color = $page->getAttribute('search_field_color');
$btn_cta_color = $page->getAttribute('btn_cta_color');
$links_color = $page->getAttribute('links_color');
$footer_bgcolor = $page->getAttribute('footer_bgcolor');
$events_color = $page->getAttribute('events_color');
$events_bgcolor = $page->getAttribute('events_bgcolor');
//print_r($siteController);
//exit();
//}
//print_r($site);
$sitePageId = '';
if( $site->getSiteHomePageID() != 1) {
	$sitePageId = $site->getSiteHomePageID();
}
$text2 = '@import "build/bootstrap/bootstrap.min.css";
@import "build/blocks/autonav/templates/bootstrap-dropdown/nav.less";
@import "build/mobile/navigation/slicknav.less";

/* Import our theme variables. */';
if($page) {
$logomaincolor = '#6ebb4f';
$h1titlecolor = '#6ebb4f';
$h2titlecolor = '#6ebb4f';
$h3titlecolor = '#6ebb4f';
$linkshovercolor = '#0b99c1';
$buttonctacolor = '#fe9917';
$searchfieldcolor = '#72bb45';
$monthdaycolor = '#387ea8';
$datebackgroundcolor = '#b1ccdd';
$footerbackgroundcolor = '#387ea8';
$text2 .= ($siteColor) ? "@logo-main-color:".$siteColor.";" : '@logo-main-color:#6ebb4f;';
$text2 .= ($h1_color) ?  "@h1-title-color: ".$h1_color.";" : '@h1-title-color: #6ebb4f;';
$text2 .= ($h2_color) ? "@h2-title-color: $h2_color;": '@h2-title-color: #6ebb4f;';
$text2 .= ($h3_color) ? "@h3-title-color: $h3_color;" : '@h3-title-color: #6ebb4f;';
$text2 .= ($search_field_color) ?  "@links-hover-color: ".$search_field_color.";" : '@links-hover-color: #0b99c1;';
$text2 .= ($btn_cta_color) ?  "@button-cta-color: ".$btn_cta_color.";" : '@button-cta-color: #fe9917;';
$text2 .= ($links_color) ?  "@search-field-color: ".$links_color.";" : '@search-field-color: #72bb45;';
$text2 .= ($events_color) ? "@month-day-color: ".$events_color.";" : '@month-day-color: #72bb45;';
$text2 .= ($events_bgcolor) ?  "@date-background-color: ".$events_bgcolor.";": '@date-background-color: #b1ccdd;';
$text2 .= ($footer_bgcolor) ?  "@footer-background-color: ".$footer_bgcolor.";" : '@footer-background-color: #387ea8;';
$text2 .= 'body {
}
a {
	color: @links-hover-color;
}
a:hover {
	color: @links-hover-color;
}
h1 {
	font-size: 30px;
}
h1, h2, h3, h4 {
	margin: 0 0 10px;
	font-family: "Roboto Slab", serif;
	font-weight: bold;
}
h1 {
	/* customize_headings */color: @h1-title-color;
}
h2 {
	/* customize_headings */color: @h2-title-color;
}
h3 {
	/* customize_headings */color: @h3-title-color;
}
#logo a {
	color: @logo-main-color;
}
.btn-default,
.btn-default:hover,
.header_cta {
    background-color: @button-cta-color;
}

.sb-icon-search,
.sb-search-open .sb-icon-search {
	color: @search-field-color !important;
}

.ccm-block-calendar-event-list-event-date,
.ccm-block-calendar-event-list-event-date span:first-child {
	border-color: @date-background-color;
}
.ccm-block-calendar-event-list-event-date span:first-child {
	background-color: @date-background-color;
}
.ccm-block-calendar-event-list-event-date {
	color: @month-day-color;
}
#footer {
    background-color: @footer-background-color;
}';
$fp = fopen('/var/www/html/concrete/application/themes/library_theme/css/main'.$site->getSiteID().'.less', 'w');
fwrite($fp, $text2);
fclose($fp);
}
if($c->isEditmode()) { ?>
   <style type="text/css">
    #home #header {
      position: relative;
    }
  	#main-navigation {
  	    clear: both;
  	}
    .slide-bg {display: none;}
   </style>
<?php } 

?>
<link href="<?=$view->getStylesheet('main'.$site->getSiteID().'.less')?>" rel='stylesheet' type='text/css'>
</head>

<?php
$th = Loader::helper('text');
$class = $th->sanitizeFileSystem($c->getCollectionName(), $leaveSlashes=false);
?>
<body id="<?php print $class." ".$site->getSiteHomePageID(); ?>" class="<?= $c->getPageTemplateHandle() ?>" style="background:<?php //echo $siteColor;?>;">
<div class="<?= $c->getPageWrapperClass() ?>">
