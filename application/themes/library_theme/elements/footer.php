<?php     defined('C5_EXECUTE') or die(_("Access Denied.")); 
$page = Page::getCurrentPage();
$app = \Concrete\Core\Support\Facade\Application::getFacadeApplication();
$service = $app->make('site');
$site = $service->getSite();
$sitePageId = '';
if( $site->getSiteHomePageID() != 1) {
	$sitePageId = $site->getSiteHomePageID();
}
?>
<div class="clearfix"></div>
    <footer>
    <div id="footer">
        <div class="container">
            <div class="row">
                <div class="col-sm-3">
                    <div class="fcol">
                        <?php        
                            $a = new GlobalArea('Footer Column 1'.$sitePageId);
                            $a->display($c); // footer editable region
                        ?> 
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="fcol">
                        <?php        
                            $a = new GlobalArea('Footer Content 2'.$sitePageId);
                            $a->display($c); // footer editable region
                        ?> 
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="fcol">
                        <?php        
                            $a = new GlobalArea('Footer Content 3'.$sitePageId);
                            $a->display($c); // footer editable region
                        ?> 
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="fcol">
                        <?php        
                            $a = new GlobalArea('Footer Content 4'.$sitePageId);
                            $a->display($c); // footer editable region
                        ?>  
                    </div>
                </div>
            </div>
            <div class="row">    
                <div class="col-xs-12">  
                    <div class="footer-bottom">
                        <?php        
                            $a = new GlobalArea('Footer Bottom Info'.$sitePageId);
                            $a->display($c); // footer editable region
                        ?>   
                        <?php $site = Config::get('concrete.site'); ?>
                        <p class="footer-copyright">Copyright <?php echo date('Y')?> <a href="<?php echo DIR_REL?>/"><?php echo $site; ?></a></p><!-- /.copyright --> 
                    </div>
                </div>          
            </div>
        </div>
    </div>  
    <!-- <div id="elevator_item" style="display: block;"> 
        <a id="elevator" onclick="return false;" title="Back To Top"></a> 
    </div> -->
    </footer> <!-- close footer -->


<?php  $this->inc('elements/footer_bottom.php'); // get header file ?>
