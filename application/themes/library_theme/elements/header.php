<?php     defined('C5_EXECUTE') or die(_("Access Denied.")); 
$this->inc('elements/header_top.php'); // get header file 
$page = Page::getCurrentPage();
$app = \Concrete\Core\Support\Facade\Application::getFacadeApplication();
$service = $app->make('site');
$site = $service->getSite();
$sitePageId = '';
if( $site->getSiteHomePageID() != 1) {
	$sitePageId = $site->getSiteHomePageID();
}
?>
  <header>
  <div id="header">
    <nav class="navibar">
      <div class="container">
        <div class="navbar-header">
          <div id="logo">
            <?php
              $a = new GlobalArea('Logo'.$sitePageId);
              if ($c->isEditMode() || $a->getTotalBlocksInArea($c) > 0 ) {
                $a->display($c);
              }
              else {
                $site = Config::get('concrete.site');
                ?><a href="<?php echo DIR_REL?>/"><?php echo $site; ?></a><?php
              }
            ?>
          </div>
        </div>
        <div class="top-header">
          <div class="header_address">
            <?php       
                $a = new GlobalArea('Contact'.$sitePageId);
                $a->display($c); // main editable region
            ?>           
          </div>
          <?php
            $a = new GlobalArea('CTA'.$sitePageId);
            if ($c->isEditMode() || $a->getTotalBlocksInArea($c) > 0 ) {
                ?><div class="header_cta"><?php
                $a->display($c);
                ?></div><?php
            }
          ?>
        </div>
      </div>      
      <div class="clearfix"></div>
      <div id="main-navigation">
        <div class="container">
          <?php       
              $a = new GlobalArea('Main Navigation'.$sitePageId);
              $a->setCustomTemplate('autonav', 'bootstrap-dropdown');
              $a->display($c); // main editable region
          ?> 
        </div>
      </div><!--/.nav-collapse -->
    </nav>   
  </div>
  <div id="mobile-nav"></div>
</header>
