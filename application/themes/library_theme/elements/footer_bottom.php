<?php     defined('C5_EXECUTE') or die(_("Access Denied.")); ?>
</div><!-- c57 pagewrapper class -->
<!-- this is where we put our custom functions -->
<script src="<?=$view->getThemePath()?>/js/bootstrap.min.js"></script>
<script src="<?php echo $this->getThemePath();?>/js/slicknav.js"></script><!-- 
<script src="<?php echo $this->getThemePath();?>/js/backtotop.js"></script> -->
<script src="<?php echo $this->getThemePath();?>/js/match-height.js"></script>
<script src="<?php echo $this->getThemePath(); ?>/js/theme.js"></script>
<script>
	jQuery(document).ready(function ($) {
		if ( $('div').hasClass('lr_box') ) {
			$('.lr_description').matchHeight();
		}
	});
</script>
<style>
.ccm-block-page-list-title a {
    font-weight: normal !important;
}
.breadcrumb {
    padding: 15px 8px;
    margin-bottom: 20px;
    list-style: none;
    background-color: transparent;
    border-radius: 4px;
}
.breadcrumb>li+li:before {
    padding: 0 5px;
    color: #ccc;
    content: "\00bb";
}
</style>
<?php     View::element('footer_required'); // Adds Google Analytics & gives the opportunity to output items into the footer ?>

</body>
</html>
