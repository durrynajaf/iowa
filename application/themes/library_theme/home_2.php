<?php     defined('C5_EXECUTE') or die(_("Access Denied."));
$this->inc('elements/header.php'); // get header file 
$page = Page::getCurrentPage();
$app = \Concrete\Core\Support\Facade\Application::getFacadeApplication();
$service = $app->make('site');
$site = $service->getSite();
$sitePageId = '';
if( $site->getSiteHomePageID() != 1) {
	$sitePageId = $site->getSiteHomePageID();
}
?>
<div id="home-2">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-9">
                <div id="hero">
                    <img src="<?=$view->getThemePath()?>/images/slide-top-curve.png" class="top-curve slide-bg">
                    <img src="<?=$view->getThemePath()?>/images/slide-bottom-curve.png" class="bottom-curve slide-bg">
                    <?php       
                        $a = new Area('Masthead');
                        $a->display($c); // main editable region
                    ?>
                </div>
            </div>
            <div class="col-sm-12 col-md-3">
                <div class="row">
                    <div class="home-2-hero--stack">
                        <div class="col-xs-4 col-sm-4 col-md-12">
                            <?php       
                                $a = new Area('Masthead Side 1');
                                $a->display($c); // main editable region
                            ?>
                        </div>
                        <div class="col-xs-4 col-sm-4 col-md-12">
                            <?php       
                                $a = new Area('Masthead Side 2');
                                $a->display($c); // main editable region
                            ?>
                        </div>
                        <div class="col-xs-4 col-sm-4 col-md-12">
                            <?php       
                                $a = new Area('Masthead Side 3');
                                $a->display($c); // main editable region
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="main-content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-8 col-md-9 sideline">
                <div class="home-intro col-xs-12">
                    <?php       
                        $a = new Area('Main');
                        $a->display($c); // main editable region
                    ?>
                </div>
                <div class="clearfix"></div>
                <div class="box-group">
                    <div class="boxes">
                        <div class="col-xs-6 col-sm-6 col-md-4">
                            <?php
                                $a = new Area('Box Content 1');
                                $a->setBlockLimit(1);
                                $a->display($c);
                            ?>
                        </div><!-- .col-sm-4 -->
                        <div class="col-xs-6 col-sm-6 col-md-4">
                            <?php
                                $a = new Area('Box Content 2');
                                $a->setBlockLimit(1);
                                $a->display($c);
                            ?>
                        </div><!-- .col-sm-4 -->
                        <div class="col-xs-6 col-sm-6 col-md-4">
                            <?php
                                $a = new Area('Box Content 3');
                                $a->setBlockLimit(1);
                                $a->display($c);
                            ?>
                        </div><!-- .col-sm-4 -->
                    </div><!-- .boxes.row -->
                    <div class="boxes">
                        <div class="col-xs-6 col-sm-6 col-md-4">
                            <?php
                                $a = new Area('Box Content 4');
                                $a->setBlockLimit(1);
                                $a->display($c);
                            ?>
                        </div><!-- .col-sm-4 -->
                        <div class="col-xs-6 col-sm-6 col-md-4">
                            <?php
                                $a = new Area('Box Content 5');
                                $a->setBlockLimit(1);
                                $a->display($c);
                            ?>
                        </div><!-- .col-sm-4 -->
                        <div class="col-xs-6 col-sm-6 col-md-4">
                            <?php
                                $a = new Area('Box Content 6');
                                $a->setBlockLimit(1);
                                $a->display($c);
                            ?>
                        </div><!-- .col-sm-4 -->
                    </div><!-- .boxes.row -->
                    <div class="boxes">
                        <div class="col-xs-6 col-sm-6 col-md-4">
                            <?php
                                $a = new Area('Box Content 7');
                                $a->setBlockLimit(1);
                                $a->display($c);
                            ?>
                        </div><!-- .col-sm-4 -->
                        <div class="col-xs-6 col-sm-6 col-md-4">
                            <?php
                                $a = new Area('Box Content 8');
                                $a->setBlockLimit(1);
                                $a->display($c);
                            ?>
                        </div><!-- .col-sm-4 -->
                        <div class="col-xs-6 col-sm-6 col-md-4">
                            <?php
                                $a = new Area('Box Content 9');
                                $a->setBlockLimit(1);
                                $a->display($c);
                            ?>
                        </div><!-- .col-sm-4 -->
                    </div><!-- .boxes -->
                </div><!-- .box-group -->
		        <div class="clearfix"></div>
		        <div class="col-xs-12">
                    <div class="bottom-content">
                        <?php       
                            $a = new Area('Bottom');
                            $a->display($c); // main editable region
                        ?>
                    </div><!-- .bottom-content -->
		        </div>
            </div><!-- .col-sm-8 .col-md-9 -->
            <div class="col-xs-12 col-sm-4 col-md-3">
                <aside class="sidebar">
		    <?php
                        $a = new Area('Sidebar');
                        $a->display();

                        echo "<br><br>";
                        $a = new GlobalArea('Sidebar Global'.$sitePageId);
                        $a->display($c); // sidebar editable region
                    ?>

                    <?php
                        $a = new Area('Sidebar bottom');
                        $a->display($c); // sidebar editable region
                    ?>

                    <?php       
                     //   $a = new Area('Sidebar');
                      //  $a->display($c); // main editable region
                    ?>
                </aside>
            </div>
        </div><!-- .row -->
    </div><!-- #main-content.container -->
</div><!-- #main-content -->

<?php  $this->inc('elements/footer.php'); // get footer.php ?>
