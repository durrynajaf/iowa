<?php

namespace Application\Theme\LibraryTheme;

use Concrete\Core\Page\Theme\Theme;
 
class PageTheme extends Theme {

    protected $pThemeGridFrameworkHandle = 'bootstrap3';

	public function registerAssets() 
	{ 
		$this->providesAsset('javascript', 'bootstrap/*'); 
		$this->providesAsset('css', 'bootstrap/*');
		$this->providesAsset('css', 'core/frontend/*');
		$this->requireAsset('javascript', 'jquery'); 
	}
	public function getThemeName()
    {
        return t('Library Theme');
    }

    public function getThemeDescription()
    {
        return t('Responsive Concrete5 Iowa Library Theme.');
    }
	public function getThemeEditorClasses()
	{
	    return array(
            array('title' => t('Solid Button'), 'menuClass' => 'btn-default', 'spanClass' => 'btn btn-default'),
	    );
	}
	public function getThemeBlockClasses()
	{
	    return array(
	        'content' => array('blocks', 'section-2-title', 'op-white-bg')
	    );
	}
	public function getThemeAreaClasses()
	{
	    return array(
	        'Section 3' => array('op-white-bg','box')
	    );
	}
}


