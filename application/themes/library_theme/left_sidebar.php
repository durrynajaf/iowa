<?php     defined('C5_EXECUTE') or die(_("Access Denied."));
$this->inc('elements/header.php'); // get header file ?>
<div class="container">

    <?php
$page = Page::getCurrentPage();
$app = \Concrete\Core\Support\Facade\Application::getFacadeApplication();
$service = $app->make('site');
$site = $service->getSite();
$sitePageId = '';
if( $site->getSiteHomePageID() != 1) {
	$sitePageId = $site->getSiteHomePageID();
}
$a = new GlobalArea('Bread Crumb'.$sitePageId);
$a->display();
?>

        </div>

<div id="page-content">
 
    <div class="container">
        <div class="row reverse">
            <div class="col-xs-12 col-sm-8 col-md-9 sideline">
                <article>
                    <?php       
                        $a = new Area('Main');
                        $a->enableGridContainer();
                        $a->display($c); // main editable region
                    ?>
                </article> <!-- close 1st article -->              
            </div>
            <div class="col-xs-12 col-sm-4 col-md-3">
                <aside>
		    <?php 
			$a = new Area('Sidebar');
			$a->display();
		    	
			echo "<br><br>";
                        $a = new GlobalArea('Sidebar Global'.$sitePageId);
                        $a->display($c); // sidebar editable region
                    ?>
	
                    <?php       
                        $a = new Area('Sidebar bottom');
                        $a->display($c); // sidebar editable region
                    ?>
                </aside> <!-- close aside -->               
            </div>
        </div>
    </div>
</div>

<?php  $this->inc('elements/footer.php'); // get footer.php ?>
