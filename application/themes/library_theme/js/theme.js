jQuery(document).ready(function ($) {

        // Slicknav trigger js for mobile navigation
        $('#menu').slicknav({
            label: '',
            prependTo:'#mobile-nav',
            closedSymbol: '<i class="fa fa-chevron-right" aria-hidden="true"></i>',
            openedSymbol: '<i class="fa fa-chevron-down" aria-hidden="true"></i>'
        });

        //touch dropdown js
        var deviceAgent = navigator.userAgent.toLowerCase();

        var isTouchDevice = Modernizr.touch || 
        (deviceAgent.match(/(iphone|ipod|ipad)/) ||
        deviceAgent.match(/(android)/)  || 
        deviceAgent.match(/(iemobile)/) || 
        deviceAgent.match(/iphone/i) || 
        deviceAgent.match(/ipad/i) || 
        deviceAgent.match(/ipod/i) || 
        deviceAgent.match(/blackberry/i) || 
        deviceAgent.match(/bada/i));

        if (isTouchDevice) {
        //Do something touchy
            $('#menu li').children('ul').hide();
            $('#menu li a').click(function (event) { 
                var ts=$(this);
             var len=$(ts).parent('li').has('ul').length;
               if(len>0)
               {
                   if($(ts).hasClass('clicked'))
                   {
                       
                   }
                   else
                   {
                       $(ts).parent('li').find('ul').first().slideDown();
                       $(ts).addClass('clicked');
                       return false;
                   }
               }
            })
        }
    
});
