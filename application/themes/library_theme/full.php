<?php     defined('C5_EXECUTE') or die(_("Access Denied."));
$this->inc('elements/header.php'); // get header file ?>
<div class="container">
    <?php
$page = Page::getCurrentPage();
$app = \Concrete\Core\Support\Facade\Application::getFacadeApplication();
$service = $app->make('site');
$site = $service->getSite();
$sitePageId = '';
if( $site->getSiteHomePageID() != 1) {
	$sitePageId = $site->getSiteHomePageID();
}
$a = new GlobalArea('Bread Crumb'.$sitePageId);
$a->display();
?>

        </div>

<div id="page-content">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
			    <article>
			        <?php       
			            $a = new Area('Main');
			            $a->enableGridContainer();
			            $a->display($c); // main editable region
			        ?>
			    </article> <!-- close 1st article -->			
			</div>
		</div>
	</div>
</div>


<?php  $this->inc('elements/footer.php'); // get footer.php ?>
