<?php

return [

		'theme_paths'         => array(
			'/register'      => 'library_theme',
			'/register/*'      => 'library_theme',
			'/login'      => 'library_theme',
			'/login/*'      => 'library_theme',
			'/account'      => 'library_theme',
			'/account/*'      => 'library_theme',
			'/page_forbidden'      => 'library_theme',
			'/page_not_found'      => 'library_theme'
	    ),

];
