<?php 
/* Cachekey: cache/stash_default/zend/zend_feed_reader_32cf6f1b9ec7fdf83da2b96ed1f32a3c/ */
/* Type: array */
/* Expiration: 2019-07-30T12:11:39+00:00 */



$loaded = true;
$expiration = 1564488699;

$data = array();

/* Child Type: string */
$data['return'] = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>
<rss version=\"2.0\" xmlns:slash=\"http://purl.org/rss/1.0/modules/slash/\">
  <channel>
    <title>concrete5.org Blog</title>
    <description>concrete5.org Blog</description>
    <generator>Zend_Feed_Writer 2 (http://framework.zend.com)</generator>
    <link>https://www.concrete5.org/about/blog</link>
    <item>
      <title>Learning concrete5, season 2, episode 3: 2019 July 11</title>
      <description><![CDATA[Learning concrete5 continues by finishing up the headers and footers from the last episode and begin creating some menu functionality.]]></description>
      <pubDate>Thu, 11 Jul 2019 01:30:00 +0000</pubDate>
      <link>https://www.concrete5.org/about/blog/community-blog/learning-concrete5-season-2-episode-3-2019-july-11</link>
      <guid>https://www.concrete5.org/about/blog/community-blog/learning-concrete5-season-2-episode-3-2019-july-11</guid>
      <slash:comments>0</slash:comments>
    </item>
    <item>
      <title>You are invited to our July 2019 Town hall meeting</title>
      <description><![CDATA[Join us for another online town hall meeting on Tuesday (2019/07/02, 2019 July 02) at 9:30am Pacific Daylight Time.]]></description>
      <pubDate>Tue, 02 Jul 2019 04:12:00 +0000</pubDate>
      <link>https://www.concrete5.org/about/blog/community-blog/you-are-invited-our-july-2019-town-hall-meeting</link>
      <guid>https://www.concrete5.org/about/blog/community-blog/you-are-invited-our-july-2019-town-hall-meeting</guid>
      <slash:comments>0</slash:comments>
    </item>
    <item>
      <title>Learning concrete5, season 2, episode 2: 2019 June 27</title>
      <description><![CDATA[Learning concrete5 continues with the beginnings of a theme: page templates, headers, and footers]]></description>
      <pubDate>Wed, 26 Jun 2019 01:30:00 +0000</pubDate>
      <link>https://www.concrete5.org/about/blog/community-blog/learning-concrete5-season-2-episode-2-2019-june-27</link>
      <guid>https://www.concrete5.org/about/blog/community-blog/learning-concrete5-season-2-episode-2-2019-june-27</guid>
      <slash:comments>0</slash:comments>
    </item>
    <item>
      <title>Learning concrete5, season 2, episode 1 live on 2019 June 13</title>
      <description><![CDATA[We're kicking off a new season of Learning concrete5! After the first few episodes, we've decided to bring more of the team onboard to share even more details of the steps we go through when developing with concrete5.]]></description>
      <pubDate>Thu, 13 Jun 2019 16:30:00 +0000</pubDate>
      <link>https://www.concrete5.org/about/blog/community-blog/learning-concrete5-season-2-episode-1-live-2019-june-13</link>
      <guid>https://www.concrete5.org/about/blog/community-blog/learning-concrete5-season-2-episode-1-live-2019-june-13</guid>
      <slash:comments>0</slash:comments>
    </item>
    <item>
      <title>ISO 27001:2013 Certification</title>
      <description><![CDATA[ and what it means to the concrete5 community.]]></description>
      <pubDate>Fri, 07 Jun 2019 01:53:00 +0000</pubDate>
      <link>https://www.concrete5.org/about/blog/news/iso-270012013-certification</link>
      <guid>https://www.concrete5.org/about/blog/news/iso-270012013-certification</guid>
      <slash:comments>0</slash:comments>
    </item>
    <item>
      <title>You are invited to our June 2019 Town hall meeting</title>
      <description><![CDATA[Join us for another online town hall meeting on Tuesday (2019/06/04, 2019 June 04) at 9:30am Pacific Daylight Time.]]></description>
      <pubDate>Fri, 31 May 2019 22:43:00 +0000</pubDate>
      <link>https://www.concrete5.org/about/blog/community-blog/you-are-invited-our-june-2019-town-hall-meeting</link>
      <guid>https://www.concrete5.org/about/blog/community-blog/you-are-invited-our-june-2019-town-hall-meeting</guid>
      <slash:comments>0</slash:comments>
    </item>
    <item>
      <title>Join us for another developer chat 2019 May 23</title>
      <description><![CDATA[It's time for another episode Learning concrete5 with Korvin and Myq. We are holding an ongoing series of interactive sessions where concrete5 community developers can interact directly with members of the core team to discuss best practices and explore challenging problems.]]></description>
      <pubDate>Wed, 22 May 2019 02:10:00 +0000</pubDate>
      <link>https://www.concrete5.org/about/blog/community-blog/join-us-another-developer-chat</link>
      <guid>https://www.concrete5.org/about/blog/community-blog/join-us-another-developer-chat</guid>
      <slash:comments>0</slash:comments>
    </item>
    <item>
      <title>C5 Dev Round Up #2</title>
      <description><![CDATA[It's time our second some developer network tutorials! We are holding an ongoing series of interactive sessions where concrete5 community developers can interact directly with members of the core team to discuss best practices and explore challenging problems.]]></description>
      <pubDate>Thu, 09 May 2019 16:08:00 +0000</pubDate>
      <link>https://www.concrete5.org/about/blog/community-blog/second-developer-network-tutorial</link>
      <guid>https://www.concrete5.org/about/blog/community-blog/second-developer-network-tutorial</guid>
      <slash:comments>0</slash:comments>
    </item>
    <item>
      <title>Join us for our May Town Hall meeting</title>
      <description><![CDATA[Join us for another online town hall meeting on Tuesday (2019/05/07, 2019 May 07) at 9:30am Pacific Daylight Time.]]></description>
      <pubDate>Mon, 06 May 2019 16:13:00 +0000</pubDate>
      <link>https://www.concrete5.org/about/blog/community-blog/join-us-our-may-town-hall-meeting</link>
      <guid>https://www.concrete5.org/about/blog/community-blog/join-us-our-may-town-hall-meeting</guid>
      <slash:comments>0</slash:comments>
    </item>
    <item>
      <title>HereNT - In Memoriam</title>
      <description><![CDATA[We've lost a great community member]]></description>
      <pubDate>Fri, 26 Apr 2019 19:48:00 +0000</pubDate>
      <link>https://www.concrete5.org/about/blog/community-blog/herent-memoriam</link>
      <guid>https://www.concrete5.org/about/blog/community-blog/herent-memoriam</guid>
      <slash:comments>0</slash:comments>
    </item>
    <item>
      <title>First developer network tutorial</title>
      <description><![CDATA[No Content.]]></description>
      <pubDate>Wed, 24 Apr 2019 22:38:00 +0000</pubDate>
      <link>https://www.concrete5.org/about/blog/community-blog/first-developer-network-tutorial</link>
      <guid>https://www.concrete5.org/about/blog/community-blog/first-developer-network-tutorial</guid>
      <slash:comments>0</slash:comments>
    </item>
    <item>
      <title>concrete5 8.5.1 is Now Available!</title>
      <description><![CDATA[The first maintenance release of 8.5.0 fixes a number of bugs.]]></description>
      <pubDate>Wed, 03 Apr 2019 21:31:00 +0000</pubDate>
      <link>https://www.concrete5.org/about/blog/core-releases/concrete5-851-now-available</link>
      <guid>https://www.concrete5.org/about/blog/core-releases/concrete5-851-now-available</guid>
      <slash:comments>0</slash:comments>
    </item>
    <item>
      <title>Join us for our April Town Hall meeting</title>
      <description><![CDATA[No Content.]]></description>
      <pubDate>Mon, 01 Apr 2019 15:10:00 +0000</pubDate>
      <link>https://www.concrete5.org/about/blog/community-blog/join-us-our-april-town-hall-meeting</link>
      <guid>https://www.concrete5.org/about/blog/community-blog/join-us-our-april-town-hall-meeting</guid>
      <slash:comments>0</slash:comments>
    </item>
    <item>
      <title>concrete5 8.5.0 is Now Available!</title>
      <description><![CDATA[Nearly a year in the making, concrete5 8.5.0 is now available today! Happy Pi Day!]]></description>
      <pubDate>Thu, 14 Mar 2019 22:33:00 +0000</pubDate>
      <link>https://www.concrete5.org/about/blog/core-releases/concrete5-850-now-available</link>
      <guid>https://www.concrete5.org/about/blog/core-releases/concrete5-850-now-available</guid>
      <slash:comments>0</slash:comments>
    </item>
    <item>
      <title>Town Hall Meeting - 2019 March 5th</title>
      <description><![CDATA[Join us to discuss concrete5]]></description>
      <pubDate>Mon, 04 Mar 2019 22:06:00 +0000</pubDate>
      <link>https://www.concrete5.org/about/blog/community-blog/town-hall-meeting-2019-march-05</link>
      <guid>https://www.concrete5.org/about/blog/community-blog/town-hall-meeting-2019-march-05</guid>
      <slash:comments>0</slash:comments>
    </item>
    <item>
      <title>concrete5 8.4.5, 8.5.0RC2 and 5.6.4.0 Released</title>
      <description><![CDATA[No Content.]]></description>
      <pubDate>Tue, 26 Feb 2019 23:20:00 +0000</pubDate>
      <link>https://www.concrete5.org/about/blog/core-releases/concrete5-845-850rc2-and-5640-released</link>
      <guid>https://www.concrete5.org/about/blog/core-releases/concrete5-845-850rc2-and-5640-released</guid>
      <slash:comments>0</slash:comments>
    </item>
    <item>
      <title>Messaging System Vulnerability</title>
      <description><![CDATA[ If your site uses concrete5’s in-box/messaging functionality, or you have shared sensitive information in direct messages on concrete5.org read in]]></description>
      <pubDate>Tue, 26 Feb 2019 22:35:00 +0000</pubDate>
      <link>https://www.concrete5.org/about/blog/security/messaging-system-vulnerability</link>
      <guid>https://www.concrete5.org/about/blog/security/messaging-system-vulnerability</guid>
      <slash:comments>0</slash:comments>
    </item>
    <item>
      <title>Google Summer of Code</title>
      <description><![CDATA[Contribute to open source at concrete5]]></description>
      <pubDate>Tue, 05 Feb 2019 21:28:00 +0000</pubDate>
      <link>https://www.concrete5.org/about/blog/community-blog/google-summer-code</link>
      <guid>https://www.concrete5.org/about/blog/community-blog/google-summer-code</guid>
      <slash:comments>0</slash:comments>
    </item>
    <item>
      <title>Townhall meeting on Tuesday, Februray 5 / 05 February</title>
      <description><![CDATA[Meet with the core team on Tuesday]]></description>
      <pubDate>Sun, 03 Feb 2019 00:23:00 +0000</pubDate>
      <link>https://www.concrete5.org/about/blog/community-blog/townhall-meeting-tuesday-februray-5-05-february</link>
      <guid>https://www.concrete5.org/about/blog/community-blog/townhall-meeting-tuesday-februray-5-05-february</guid>
      <slash:comments>0</slash:comments>
    </item>
    <item>
      <title>concrete5 8.4.4 Now Available!</title>
      <description><![CDATA[No Content.]]></description>
      <pubDate>Thu, 10 Jan 2019 04:53:00 +0000</pubDate>
      <link>https://www.concrete5.org/about/blog/community-blog/concrete5-844-now-available</link>
      <guid>https://www.concrete5.org/about/blog/community-blog/concrete5-844-now-available</guid>
      <slash:comments>0</slash:comments>
    </item>
  </channel>
</rss>
";

/* Child Type: integer */
$data['createdOn'] = 1564063857;
