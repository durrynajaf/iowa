<?php namespace Application\Block\DefaultButton;

defined("C5_EXECUTE") or die("Access Denied.");

use AssetList;
use Concrete\Core\Block\BlockController;
use Core;
use File;
use Page;
use Permissions;

class Controller extends BlockController
{
    public $btFieldsRequired = [];
    protected $btTable = 'btDefaultButton';
    protected $btInterfaceWidth = 400;
    protected $btInterfaceHeight = 500;
    protected $btIgnorePageThemeGridFrameworkContainer = false;
    protected $btCacheBlockRecord = true;
    protected $btCacheBlockOutput = true;
    protected $btCacheBlockOutputOnPost = true;
    protected $btCacheBlockOutputForRegisteredUsers = true;
    protected $pkg = false;
    
    public function getBlockTypeName()
    {
        return t("Default Button");
    }

    public function view()
    {
        $btnalign_options = [
            '' => "-- " . t("None") . " --",
            '1' => "center",
            '2' => "left",
            '3' => "right"
        ];
        $this->set("btnalign_options", $btnalign_options);
        $btnlink_URL = null;
		$btnlink_Object = null;
		$btnlink_Title = trim($this->btnlink_Title);
		if (trim($this->btnlink) != '') {
			switch ($this->btnlink) {
				case 'page':
					if ($this->btnlink_Page > 0 && ($btnlink_Page_c = Page::getByID($this->btnlink_Page)) && !$btnlink_Page_c->error && !$btnlink_Page_c->isInTrash()) {
						$btnlink_Object = $btnlink_Page_c;
						$btnlink_URL = $btnlink_Page_c->getCollectionLink();
						if ($btnlink_Title == '') {
							$btnlink_Title = $btnlink_Page_c->getCollectionName();
						}
					}
					break;
				case 'file':
					$btnlink_File_id = (int)$this->btnlink_File;
					if ($btnlink_File_id > 0 && ($btnlink_File_object = File::getByID($btnlink_File_id)) && is_object($btnlink_File_object)) {
						$fp = new Permissions($btnlink_File_object);
						if ($fp->canViewFile()) {
							$btnlink_Object = $btnlink_File_object;
							$btnlink_URL = $btnlink_File_object->getRelativePath();
							if ($btnlink_Title == '') {
								$btnlink_Title = $btnlink_File_object->getTitle();
							}
						}
					}
					break;
				case 'url':
					$btnlink_URL = $this->btnlink_URL;
					if ($btnlink_Title == '') {
						$btnlink_Title = $btnlink_URL;
					}
					break;
				case 'relative_url':
					$btnlink_URL = $this->btnlink_Relative_URL;
					if ($btnlink_Title == '') {
						$btnlink_Title = $this->btnlink_Relative_URL;
					}
					break;
			}
		}
		$this->set("btnlink_URL", $btnlink_URL);
		$this->set("btnlink_Object", $btnlink_Object);
		$this->set("btnlink_Title", $btnlink_Title);
    }

    public function add()
    {
        $this->addEdit();
    }

    public function edit()
    {
        $this->addEdit();
    }

    protected function addEdit()
    {
        $this->set("btnalign_options", [
                '' => "-- " . t("None") . " --",
                '1' => "center",
                '2' => "left",
                '3' => "right"
            ]
        );
        $this->set("btnlink_Options", $this->getSmartLinkTypeOptions([
  'page',
  'file',
  'url',
  'relative_url',
], true));
        $this->set('btFieldsRequired', $this->btFieldsRequired);
        $this->set('identifier_getString', Core::make('helper/validation/identifier')->getString(18));
    }

    public function save($args)
    {
        if (isset($args["btnlink"]) && trim($args["btnlink"]) != '') {
			switch ($args["btnlink"]) {
				case 'page':
					$args["btnlink_File"] = '0';
					$args["btnlink_URL"] = '';
					$args["btnlink_Relative_URL"] = '';
					$args["btnlink_Image"] = '0';
					break;
				case 'file':
					$args["btnlink_Page"] = '0';
					$args["btnlink_URL"] = '';
					$args["btnlink_Relative_URL"] = '';
					$args["btnlink_Image"] = '0';
					break;
				case 'url':
					$args["btnlink_Page"] = '0';
					$args["btnlink_Relative_URL"] = '';
					$args["btnlink_File"] = '0';
					$args["btnlink_Image"] = '0';
					break;
				case 'relative_url':
					$args["btnlink_Page"] = '0';
					$args["btnlink_URL"] = '';
					$args["btnlink_File"] = '0';
					$args["btnlink_Image"] = '0';
					break;
				default:
					$args["btnlink_Title"] = '';
					$args["btnlink_Page"] = '0';
					$args["btnlink_File"] = '0';
					$args["btnlink_URL"] = '';
					$args["btnlink_Relative_URL"] = '';
					$args["btnlink_Image"] = '0';
					break;	
			}
		}
		else {
			$args["btnlink_Title"] = '';
			$args["btnlink_Page"] = '0';
			$args["btnlink_File"] = '0';
			$args["btnlink_URL"] = '';
			$args["btnlink_Relative_URL"] = '';
			$args["btnlink_Image"] = '0';
		}
        parent::save($args);
    }

    public function validate($args)
    {
        $e = Core::make("helper/validation/error");
        if ((in_array("btnalign", $this->btFieldsRequired) && (!isset($args["btnalign"]) || trim($args["btnalign"]) == "")) || (isset($args["btnalign"]) && trim($args["btnalign"]) != "" && !in_array($args["btnalign"], ["1", "2", "3"]))) {
            $e->add(t("The %s field has an invalid value.", t("Button Alignment")));
        }
        if ((in_array("btnlink", $this->btFieldsRequired) && (!isset($args["btnlink"]) || trim($args["btnlink"]) == "")) || (isset($args["btnlink"]) && trim($args["btnlink"]) != "" && !array_key_exists($args["btnlink"], $this->getSmartLinkTypeOptions(['page', 'file', 'url', 'relative_url'])))) {
			$e->add(t("The %s field has an invalid value.", t("Button Link")));
		} elseif (array_key_exists($args["btnlink"], $this->getSmartLinkTypeOptions(['page', 'file', 'url', 'relative_url']))) {
			switch ($args["btnlink"]) {
				case 'page':
					if (!isset($args["btnlink_Page"]) || trim($args["btnlink_Page"]) == "" || $args["btnlink_Page"] == "0" || (($page = Page::getByID($args["btnlink_Page"])) && $page->error !== false)) {
						$e->add(t("The %s field for '%s' is required.", t("Page"), t("Button Link")));
					}
					break;
				case 'file':
					if (!isset($args["btnlink_File"]) || trim($args["btnlink_File"]) == "" || !is_object(File::getByID($args["btnlink_File"]))) {
						$e->add(t("The %s field for '%s' is required.", t("File"), t("Button Link")));
					}
					break;
				case 'url':
					if (!isset($args["btnlink_URL"]) || trim($args["btnlink_URL"]) == "" || !filter_var($args["btnlink_URL"], FILTER_VALIDATE_URL)) {
						$e->add(t("The %s field for '%s' does not have a valid URL.", t("URL"), t("Button Link")));
					}
					break;
				case 'relative_url':
					if (!isset($args["btnlink_Relative_URL"]) || trim($args["btnlink_Relative_URL"]) == "") {
						$e->add(t("The %s field for '%s' is required.", t("Relative URL"), t("Button Link")));
					}
					break;	
			}
		}
        return $e;
    }

    public function composer()
    {
        $al = AssetList::getInstance();
        $al->register('javascript', 'auto-js-' . $this->btHandle, 'blocks/' . $this->btHandle . '/auto.js', [], $this->pkg);
        $this->requireAsset('javascript', 'auto-js-' . $this->btHandle);
        $this->edit();
    }

    protected function getSmartLinkTypeOptions($include = [], $checkNone = false)
	{
		$options = [
			''             => sprintf("-- %s--", t("None")),
			'page'         => t("Page"),
			'url'          => t("External URL"),
			'relative_url' => t("Relative URL"),
			'file'         => t("File"),
			'image'        => t("Image")
		];
		if ($checkNone) {
            $include = array_merge([''], $include);
        }
		$return = [];
		foreach($include as $v){
		    if(isset($options[$v])){
		        $return[$v] = $options[$v];
		    }
		}
		return $return;
	}
}