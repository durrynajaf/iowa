<?php defined("C5_EXECUTE") or die("Access Denied."); ?>

<div class="form-group">
    <?php echo $form->label($view->field('btnalign'), t("Button Alignment")); ?>
    <?php echo isset($btFieldsRequired) && in_array('btnalign', $btFieldsRequired) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
    <?php echo $form->select($view->field('btnalign'), $btnalign_options, $btnalign, []); ?>
</div>

<?php $btnlink_ContainerID = 'btDefaultButton-btnlink-container-' . $identifier_getString; ?>
<div class="ft-smart-link" id="<?php echo $btnlink_ContainerID; ?>">
	<div class="form-group">
		<?php echo $form->label($view->field('btnlink'), t("Button Link")); ?>
	    <?php echo isset($btFieldsRequired) && in_array('btnlink', $btFieldsRequired) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
	    <?php echo $form->select($view->field('btnlink'), $btnlink_Options, $btnlink, array (
  'class' => 'form-control ft-smart-link-type',
)); ?>
	</div>
	
	<div class="form-group">
		<div class="ft-smart-link-options hidden" style="padding-left: 10px;">
			<div class="form-group">
				<?php echo $form->label($view->field('btnlink_Title'), t("Title")); ?>
			    <?php echo $form->text($view->field('btnlink_Title'), $btnlink_Title, []); ?>		
			</div>
			
			<div class="form-group hidden" data-link-type="page">
			<?php echo $form->label($view->field('btnlink_Page'), t("Page")); ?>
            <small class="required"><?php echo t('Required'); ?></small>
            <?php echo Core::make("helper/form/page_selector")->selectPage($view->field('btnlink_Page'), $btnlink_Page); ?>
		</div>

		<div class="form-group hidden" data-link-type="url">
			<?php echo $form->label($view->field('btnlink_URL'), t("URL")); ?>
            <small class="required"><?php echo t('Required'); ?></small>
            <?php echo $form->text($view->field('btnlink_URL'), $btnlink_URL, []); ?>
		</div>

		<div class="form-group hidden" data-link-type="relative_url">
			<?php echo $form->label($view->field('btnlink_Relative_URL'), t("URL")); ?>
            <small class="required"><?php echo t('Required'); ?></small>
            <?php echo $form->text($view->field('btnlink_Relative_URL'), $btnlink_Relative_URL, []); ?>
		</div>

		<div class="form-group hidden" data-link-type="file">
			<?php
			if ($btnlink_File > 0) {
				$btnlink_File_o = File::getByID($btnlink_File);
				if (!is_object($btnlink_File_o)) {
					unset($btnlink_File_o);
				}
			} ?>
		    <?php echo $form->label($view->field('btnlink_File'), t("File")); ?>
            <small class="required"><?php echo t('Required'); ?></small>
            <?php echo Core::make("helper/concrete/asset_library")->file('ccm-b-default_button-btnlink_File-' . $identifier_getString, $view->field('btnlink_File'), t("Choose File"), $btnlink_File_o); ?>	
		</div>
		</div>
	</div>
</div>


<script type="text/javascript">
	Concrete.event.publish('btDefaultButton.btnlink.open', {id: '<?php echo $btnlink_ContainerID; ?>'});
	$('#<?php echo $btnlink_ContainerID; ?> .ft-smart-link-type').trigger('change');
</script>