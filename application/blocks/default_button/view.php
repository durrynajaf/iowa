<?php defined("C5_EXECUTE") or die("Access Denied."); ?>
<div class="align-button <?php if (isset($btnalign) && trim($btnalign) != "" && array_key_exists($btnalign, $btnalign_options)) { ?><?php echo $btnalign_options[$btnalign]; ?><?php } ?>">
<button class="btn btn-default">
<?php
if (trim($btnlink_URL) != "") { ?>
    <?php
	$btnlink_Attributes = [];
	$btnlink_Attributes['href'] = $btnlink_URL;
	$btnlink_AttributesHtml = join(' ', array_map(function ($key) use ($btnlink_Attributes) {
		return $key . '="' . $btnlink_Attributes[$key] . '"';
	}, array_keys($btnlink_Attributes)));
	echo sprintf('<a %s>%s</a>', $btnlink_AttributesHtml, $btnlink_Title); ?><?php
} ?>
</button>
</div>