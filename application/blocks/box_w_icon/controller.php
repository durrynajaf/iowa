<?php namespace Application\Block\BoxWIcon;

defined("C5_EXECUTE") or die("Access Denied.");

use AssetList;
use Concrete\Core\Block\BlockController;
use Core;
use Page;

class Controller extends BlockController
{
    public $btFieldsRequired = ['boxlink', 'boxlabel'];
    protected $btTable = 'btBoxWIcon';
    protected $btInterfaceWidth = 400;
    protected $btInterfaceHeight = 500;
    protected $btIgnorePageThemeGridFrameworkContainer = false;
    protected $btCacheBlockRecord = true;
    protected $btCacheBlockOutput = true;
    protected $btCacheBlockOutputOnPost = true;
    protected $btCacheBlockOutputForRegisteredUsers = true;
    protected $pkg = false;
    
    public function getBlockTypeDescription()
    {
        return t("Box with Icon and Link");
    }

    public function getBlockTypeName()
    {
        return t("Icon Box");
    }

    public function getSearchableContent()
    {
        $content = [];
        $content[] = $this->boxlabel;
        $content[] = $this->boxicon;
        return implode(" ", $content);
    }

    public function view()
    {
        $boxlink_URL = null;
		$boxlink_Object = null;
		$boxlink_Title = trim($this->boxlink_Title);
		if (trim($this->boxlink) != '') {
			switch ($this->boxlink) {
				case 'page':
					if ($this->boxlink_Page > 0 && ($boxlink_Page_c = Page::getByID($this->boxlink_Page)) && !$boxlink_Page_c->error && !$boxlink_Page_c->isInTrash()) {
						$boxlink_Object = $boxlink_Page_c;
						$boxlink_URL = $boxlink_Page_c->getCollectionLink();
						if ($boxlink_Title == '') {
							$boxlink_Title = $boxlink_Page_c->getCollectionName();
						}
					}
					break;
				case 'url':
					$boxlink_URL = $this->boxlink_URL;
					if ($boxlink_Title == '') {
						$boxlink_Title = $boxlink_URL;
					}
					break;
				case 'relative_url':
					$boxlink_URL = $this->boxlink_Relative_URL;
					if ($boxlink_Title == '') {
						$boxlink_Title = $this->boxlink_Relative_URL;
					}
					break;
			}
		}
		$this->set("boxlink_URL", $boxlink_URL);
		$this->set("boxlink_Object", $boxlink_Object);
		$this->set("boxlink_Title", $boxlink_Title);
    }

    public function add()
    {
        $this->addEdit();
    }

    public function edit()
    {
        $this->addEdit();
    }

    protected function addEdit()
    {
        $this->set("boxlink_Options", $this->getSmartLinkTypeOptions([
  'page',
  'url',
  'relative_url',
], true));
        $this->requireAsset('core/colorpicker');
        $this->set('btFieldsRequired', $this->btFieldsRequired);
        $this->set('identifier_getString', Core::make('helper/validation/identifier')->getString(18));
    }

    public function save($args)
    {
        if (isset($args["boxlink"]) && trim($args["boxlink"]) != '') {
			switch ($args["boxlink"]) {
				case 'page':
					$args["boxlink_File"] = '0';
					$args["boxlink_URL"] = '';
					$args["boxlink_Relative_URL"] = '';
					$args["boxlink_Image"] = '0';
					break;
				case 'url':
					$args["boxlink_Page"] = '0';
					$args["boxlink_Relative_URL"] = '';
					$args["boxlink_File"] = '0';
					$args["boxlink_Image"] = '0';
					break;
				case 'relative_url':
					$args["boxlink_Page"] = '0';
					$args["boxlink_URL"] = '';
					$args["boxlink_File"] = '0';
					$args["boxlink_Image"] = '0';
					break;
				default:
					$args["boxlink_Title"] = '';
					$args["boxlink_Page"] = '0';
					$args["boxlink_File"] = '0';
					$args["boxlink_URL"] = '';
					$args["boxlink_Relative_URL"] = '';
					$args["boxlink_Image"] = '0';
					break;	
			}
		}
		else {
			$args["boxlink_Title"] = '';
			$args["boxlink_Page"] = '0';
			$args["boxlink_File"] = '0';
			$args["boxlink_URL"] = '';
			$args["boxlink_Relative_URL"] = '';
			$args["boxlink_Image"] = '0';
		}
        parent::save($args);
    }

    public function validate($args)
    {
        $e = Core::make("helper/validation/error");
        if (in_array("boxbgcolor", $this->btFieldsRequired) && (trim($args["boxbgcolor"]) == "")) {
            $e->add(t("The %s field is required.", t("Box Background Color")));
        }
        if ((in_array("boxlink", $this->btFieldsRequired) && (!isset($args["boxlink"]) || trim($args["boxlink"]) == "")) || (isset($args["boxlink"]) && trim($args["boxlink"]) != "" && !array_key_exists($args["boxlink"], $this->getSmartLinkTypeOptions(['page', 'url', 'relative_url'])))) {
			$e->add(t("The %s field has an invalid value.", t("Box Link")));
		} elseif (array_key_exists($args["boxlink"], $this->getSmartLinkTypeOptions(['page', 'url', 'relative_url']))) {
			switch ($args["boxlink"]) {
				case 'page':
					if (!isset($args["boxlink_Page"]) || trim($args["boxlink_Page"]) == "" || $args["boxlink_Page"] == "0" || (($page = Page::getByID($args["boxlink_Page"])) && $page->error !== false)) {
						$e->add(t("The %s field for '%s' is required.", t("Page"), t("Box Link")));
					}
					break;
				case 'url':
					if (!isset($args["boxlink_URL"]) || trim($args["boxlink_URL"]) == "" || !filter_var($args["boxlink_URL"], FILTER_VALIDATE_URL)) {
						$e->add(t("The %s field for '%s' does not have a valid URL.", t("URL"), t("Box Link")));
					}
					break;
				case 'relative_url':
					if (!isset($args["boxlink_Relative_URL"]) || trim($args["boxlink_Relative_URL"]) == "") {
						$e->add(t("The %s field for '%s' is required.", t("Relative URL"), t("Box Link")));
					}
					break;	
			}
		}
        if (in_array("boxlabel", $this->btFieldsRequired) && (trim($args["boxlabel"]) == "")) {
            $e->add(t("The %s field is required.", t("Box Label")));
        }
        if (in_array("boxicon", $this->btFieldsRequired) && (trim($args["boxicon"]) == "")) {
            $e->add(t("The %s field is required.", t("Box Icon")));
        }
        return $e;
    }

    public function composer()
    {
        $al = AssetList::getInstance();
        $al->register('javascript', 'auto-js-' . $this->btHandle, 'blocks/' . $this->btHandle . '/auto.js', [], $this->pkg);
        $this->requireAsset('javascript', 'auto-js-' . $this->btHandle);
        $this->edit();
    }

    protected function getSmartLinkTypeOptions($include = [], $checkNone = false)
	{
		$options = [
			''             => sprintf("-- %s--", t("None")),
			'page'         => t("Page"),
			'url'          => t("External URL"),
			'relative_url' => t("Relative URL"),
			'file'         => t("File"),
			'image'        => t("Image")
		];
		if ($checkNone) {
            $include = array_merge([''], $include);
        }
		$return = [];
		foreach($include as $v){
		    if(isset($options[$v])){
		        $return[$v] = $options[$v];
		    }
		}
		return $return;
	}
}