<?php defined("C5_EXECUTE") or die("Access Denied."); ?>
<div class="box-content" style="background-color:
<?php if (isset($boxbgcolor) && trim($boxbgcolor) != "") { ?>
    <?php echo h($boxbgcolor); ?><?php } ?>
;">
<a <?php //This is where the link starts ?>
<?php
if (trim($boxlink_URL) != "") { ?>
    <?php
	$boxlink_Attributes = [];
	$boxlink_Attributes['href'] = $boxlink_URL;
	$boxlink_AttributesHtml = join(' ', array_map(function ($key) use ($boxlink_Attributes) {
		return $key . '="' . $boxlink_Attributes[$key] . '"';
	}, array_keys($boxlink_Attributes)));
	echo sprintf($boxlink_AttributesHtml, $boxlink_Title); ?><?php
} ?>
<?php //This is where the link closes ?>
class="box-links">
<?php if (isset($boxlabel) && trim($boxlabel) != "") { ?>
    <div class="text"><?php echo h($boxlabel); ?></div><?php } ?>
<!-- <span class="cml-icon cml-icon-<?php if (isset($boxicon) && trim($boxicon) != ""){?><?php echo h($boxicon); ?><?php }?>"></span> -->
<span class="fa-icon fas fa-<?php if (isset($boxicon) && trim($boxicon) != ""){?><?php echo h($boxicon); ?><?php }?>"></span>
</a>
</div>