<?php defined("C5_EXECUTE") or die("Access Denied."); ?>

<script type="text/javascript">
    $(function () {
        $("#boxbgcolor").spectrum({"color":false,"appendTo":".ui-dialog","containerClassName":"","replacerClassName":"","flat":false,"showInput":true,"allowEmpty":true,"showButtons":false,"clickoutFiresChange":false,"showInitial":false,"showPalette":true,"showPaletteOnly":false,"hideAfterPaletteSelect":false,"togglePaletteOnly":true,"showSelectionPalette":true,"localStorageKey":false,"preferredFormat":"hex","showAlpha":true,"disabled":false,"maxSelectionSize":7,"cancelText":"cancel","chooseText":"choose","togglePaletteMoreText":"more","togglePaletteLessText":"less","clearText":"Clear Color Selection","noColorSelectedText":"No Color Selected","theme":"sp-light","selectionPalette":[],"offset":null, hide: function(color) {
                   $('.sp-container').hide();
                },
                beforeShow: function(tinycolor) {
                    $('.sp-container').show();
                },"palette":[["#6fbb44","#fe9a18","#b1ccdd","#387ea8"]]});
    });
</script><div class="form-group">
    <?php echo $form->label($view->field('boxbgcolor'), t("Box Background Color") . ' <i class="fa fa-question-circle launch-tooltip" data-original-title="' . t("background color for the box") . '"></i>'); ?>
    <?php echo isset($btFieldsRequired) && in_array('boxbgcolor', $btFieldsRequired) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
    <?php echo $form->text($view->field('boxbgcolor'), $boxbgcolor, array (
  'placeholder' => NULL,
)); ?>
</div>

<?php $boxlink_ContainerID = 'btBoxWIcon-boxlink-container-' . $identifier_getString; ?>
<div class="ft-smart-link" id="<?php echo $boxlink_ContainerID; ?>">
	<div class="form-group">
		<?php echo $form->label($view->field('boxlink'), t("Box Link")); ?>
	    <?php echo isset($btFieldsRequired) && in_array('boxlink', $btFieldsRequired) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
	    <?php echo $form->select($view->field('boxlink'), $boxlink_Options, $boxlink, array (
  'class' => 'form-control ft-smart-link-type',
)); ?>
	</div>
	
	<div class="form-group">
		<div class="ft-smart-link-options hidden" style="padding-left: 10px;">
			<!-- <div class="form-group">
				<?php echo $form->label($view->field('boxlink_Title'), t("Title")); ?>
			    <?php echo $form->text($view->field('boxlink_Title'), $boxlink_Title, []); ?>		
			</div> -->
			
			<div class="form-group hidden" data-link-type="page">
			<?php echo $form->label($view->field('boxlink_Page'), t("Page")); ?>
            <small class="required"><?php echo t('Required'); ?></small>
            <?php echo Core::make("helper/form/page_selector")->selectPage($view->field('boxlink_Page'), $boxlink_Page); ?>
		</div>

		<div class="form-group hidden" data-link-type="url">
			<?php echo $form->label($view->field('boxlink_URL'), t("URL")); ?>
            <small class="required"><?php echo t('Required'); ?></small>
            <?php echo $form->text($view->field('boxlink_URL'), $boxlink_URL, []); ?>
		</div>

		<div class="form-group hidden" data-link-type="relative_url">
			<?php echo $form->label($view->field('boxlink_Relative_URL'), t("URL")); ?>
            <small class="required"><?php echo t('Required'); ?></small>
            <?php echo $form->text($view->field('boxlink_Relative_URL'), $boxlink_Relative_URL, []); ?>
		</div>
		</div>
	</div>
</div>


<script type="text/javascript">
	Concrete.event.publish('btBoxWIcon.boxlink.open', {id: '<?php echo $boxlink_ContainerID; ?>'});
	$('#<?php echo $boxlink_ContainerID; ?> .ft-smart-link-type').trigger('change');
</script>

<div class="form-group">
    <?php echo $form->label($view->field('boxlabel'), t("Box Label") . ' <i class="fa fa-question-circle launch-tooltip" data-original-title="' . t("Box Label or Title") . '"></i>'); ?>
    <?php echo isset($btFieldsRequired) && in_array('boxlabel', $btFieldsRequired) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
    <?php echo $form->text($view->field('boxlabel'), $boxlabel, array (
  'maxlength' => 30,
)); ?>
</div>

<div class="form-group">
    <?php echo $form->label($view->field('boxicon'), t("Box Icon") . ' <i class="fa fa-question-circle launch-tooltip" data-original-title="' . t("Find an icon here: https://fontawesome.com/icons?d=gallery&s=solid&m=free and type the name in the field") . '"></i>'); ?>
    <?php echo isset($btFieldsRequired) && in_array('boxicon', $btFieldsRequired) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
    <?php echo $form->text($view->field('boxicon'), $boxicon, array (
  'maxlength' => 255,
)); ?>
</div>