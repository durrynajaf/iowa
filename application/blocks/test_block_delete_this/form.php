<?php defined("C5_EXECUTE") or die("Access Denied."); ?>

<div class="form-group">
    <?php
    if (isset($test) && $test > 0) {
        $test_o = File::getByID($test);
        if (!is_object($test_o)) {
            unset($test_o);
        }
    } ?>
    <?php echo $form->label($view->field('test'), t("test image")); ?>
    <?php echo isset($btFieldsRequired) && in_array('test', $btFieldsRequired) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
    <?php echo Core::make("helper/concrete/asset_library")->image('ccm-b-test_block_delete_this-test-' . $identifier_getString, $view->field('test'), t("Choose Image"), $test_o); ?>
</div>