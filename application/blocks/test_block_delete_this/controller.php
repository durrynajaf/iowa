<?php namespace Application\Block\TestBlockDeleteThis;

defined("C5_EXECUTE") or die("Access Denied.");

use Concrete\Core\Block\BlockController;
use Core;
use File;
use Page;

class Controller extends BlockController
{
    public $btFieldsRequired = [];
    protected $btExportFileColumns = ['test'];
    protected $btTable = 'btTestBlockDeleteThis';
    protected $btInterfaceWidth = 400;
    protected $btInterfaceHeight = 500;
    protected $btIgnorePageThemeGridFrameworkContainer = false;
    protected $btCacheBlockRecord = true;
    protected $btCacheBlockOutput = true;
    protected $btCacheBlockOutputOnPost = true;
    protected $btCacheBlockOutputForRegisteredUsers = true;
    protected $pkg = false;
    
    public function getBlockTypeName()
    {
        return t("Test Block");
    }

    public function view()
    {
        
        if ($this->test && ($f = File::getByID($this->test)) && is_object($f)) {
            $this->set("test", $f);
        } else {
            $this->set("test", false);
        }
    }

    public function add()
    {
        $this->addEdit();
    }

    public function edit()
    {
        $this->addEdit();
    }

    protected function addEdit()
    {
        $this->requireAsset('core/file-manager');
        $this->set('btFieldsRequired', $this->btFieldsRequired);
        $this->set('identifier_getString', Core::make('helper/validation/identifier')->getString(18));
    }

    public function validate($args)
    {
        $e = Core::make("helper/validation/error");
        if (in_array("test", $this->btFieldsRequired) && (trim($args["test"]) == "" || !is_object(File::getByID($args["test"])))) {
            $e->add(t("The %s field is required.", t("test image")));
        }
        return $e;
    }

    public function composer()
    {
        $this->edit();
    }
}