<?php defined("C5_EXECUTE") or die("Access Denied."); ?>

<div class="form-group">
    <?php echo $form->label($view->field('title'), t("Title")); ?>
    <?php echo isset($btFieldsRequired) && in_array('title', $btFieldsRequired) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
    <?php echo $form->text($view->field('title'), $title, array (
  'maxlength' => 255,
)); ?>
</div>

<div class="form-group">
    <?php
    if (isset($image) && $image > 0) {
        $image_o = File::getByID($image);
        if (!is_object($image_o)) {
            unset($image_o);
        }
    } ?>
    <?php echo $form->label($view->field('image'), t("Image")); ?>
    <?php echo isset($btFieldsRequired) && in_array('image', $btFieldsRequired) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
    <?php echo Core::make("helper/concrete/asset_library")->image('ccm-b-linking_resources-image-' . $identifier_getString, $view->field('image'), t("Choose Image"), $image_o); ?>
</div>

<div class="form-group">
    <?php echo $form->label($view->field('description_1'), t("Description")); ?>
    <?php echo isset($btFieldsRequired) && in_array('description_1', $btFieldsRequired) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
    <?php echo $form->text($view->field('description_1'), $description_1, array (
  'maxlength' => 125,
)); ?>
</div>

<?php $Linkone_ContainerID = 'btLinkingResources-Linkone-container-' . $identifier_getString; ?>
<div class="ft-smart-link" id="<?php echo $Linkone_ContainerID; ?>">
	<div class="form-group">
		<?php echo $form->label($view->field('Linkone'), t("Link 1")); ?>
	    <?php echo isset($btFieldsRequired) && in_array('Linkone', $btFieldsRequired) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
	    <?php echo $form->select($view->field('Linkone'), $Linkone_Options, $Linkone, array (
  'class' => 'form-control ft-smart-link-type',
)); ?>
	</div>
	
	<div class="form-group">
		<div class="ft-smart-link-options hidden" style="padding-left: 10px;">
			<div class="form-group">
				<?php echo $form->label($view->field('Linkone_Title'), t("Title")); ?>
			    <?php echo $form->text($view->field('Linkone_Title'), $Linkone_Title, []); ?>		
			</div>
			
			<div class="form-group hidden" data-link-type="page">
			<?php echo $form->label($view->field('Linkone_Page'), t("Page")); ?>
            <small class="required"><?php echo t('Required'); ?></small>
            <?php echo Core::make("helper/form/page_selector")->selectPage($view->field('Linkone_Page'), $Linkone_Page); ?>
		</div>

		<div class="form-group hidden" data-link-type="url">
			<?php echo $form->label($view->field('Linkone_URL'), t("URL")); ?>
            <small class="required"><?php echo t('Required'); ?></small>
            <?php echo $form->text($view->field('Linkone_URL'), $Linkone_URL, []); ?>
		</div>

		<div class="form-group hidden" data-link-type="relative_url">
			<?php echo $form->label($view->field('Linkone_Relative_URL'), t("URL")); ?>
            <small class="required"><?php echo t('Required'); ?></small>
            <?php echo $form->text($view->field('Linkone_Relative_URL'), $Linkone_Relative_URL, []); ?>
		</div>
		</div>
	</div>
</div>


<script type="text/javascript">
	Concrete.event.publish('btLinkingResources.Linkone.open', {id: '<?php echo $Linkone_ContainerID; ?>'});
	$('#<?php echo $Linkone_ContainerID; ?> .ft-smart-link-type').trigger('change');
</script>

<?php $Linktwo_ContainerID = 'btLinkingResources-Linktwo-container-' . $identifier_getString; ?>
<div class="ft-smart-link" id="<?php echo $Linktwo_ContainerID; ?>">
	<div class="form-group">
		<?php echo $form->label($view->field('Linktwo'), t("Link 2")); ?>
	    <?php echo isset($btFieldsRequired) && in_array('Linktwo', $btFieldsRequired) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
	    <?php echo $form->select($view->field('Linktwo'), $Linktwo_Options, $Linktwo, array (
  'class' => 'form-control ft-smart-link-type',
)); ?>
	</div>
	
	<div class="form-group">
		<div class="ft-smart-link-options hidden" style="padding-left: 10px;">
			<div class="form-group">
				<?php echo $form->label($view->field('Linktwo_Title'), t("Title")); ?>
			    <?php echo $form->text($view->field('Linktwo_Title'), $Linktwo_Title, []); ?>		
			</div>
			
			<div class="form-group hidden" data-link-type="page">
			<?php echo $form->label($view->field('Linktwo_Page'), t("Page")); ?>
            <small class="required"><?php echo t('Required'); ?></small>
            <?php echo Core::make("helper/form/page_selector")->selectPage($view->field('Linktwo_Page'), $Linktwo_Page); ?>
		</div>

		<div class="form-group hidden" data-link-type="url">
			<?php echo $form->label($view->field('Linktwo_URL'), t("URL")); ?>
            <small class="required"><?php echo t('Required'); ?></small>
            <?php echo $form->text($view->field('Linktwo_URL'), $Linktwo_URL, []); ?>
		</div>

		<div class="form-group hidden" data-link-type="relative_url">
			<?php echo $form->label($view->field('Linktwo_Relative_URL'), t("URL")); ?>
            <small class="required"><?php echo t('Required'); ?></small>
            <?php echo $form->text($view->field('Linktwo_Relative_URL'), $Linktwo_Relative_URL, []); ?>
		</div>
		</div>
	</div>
</div>


<script type="text/javascript">
	Concrete.event.publish('btLinkingResources.Linktwo.open', {id: '<?php echo $Linktwo_ContainerID; ?>'});
	$('#<?php echo $Linktwo_ContainerID; ?> .ft-smart-link-type').trigger('change');
</script>