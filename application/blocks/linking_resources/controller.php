<?php namespace Application\Block\LinkingResources;

defined("C5_EXECUTE") or die("Access Denied.");

use AssetList;
use Concrete\Core\Block\BlockController;
use Core;
use File;
use Page;

class Controller extends BlockController
{
    public $btFieldsRequired = [];
    protected $btExportFileColumns = ['image'];
    protected $btTable = 'btLinkingResources';
    protected $btInterfaceWidth = 400;
    protected $btInterfaceHeight = 500;
    protected $btIgnorePageThemeGridFrameworkContainer = false;
    protected $btCacheBlockRecord = true;
    protected $btCacheBlockOutput = true;
    protected $btCacheBlockOutputOnPost = true;
    protected $btCacheBlockOutputForRegisteredUsers = true;
    protected $pkg = false;
    
    public function getBlockTypeName()
    {
        return t("Linking Resources");
    }

    public function getSearchableContent()
    {
        $content = [];
        $content[] = $this->title;
        $content[] = $this->description_1;
        return implode(" ", $content);
    }

    public function view()
    {
        
        if ($this->image && ($f = File::getByID($this->image)) && is_object($f)) {
            $this->set("image", $f);
        } else {
            $this->set("image", false);
        }
        $Linkone_URL = null;
		$Linkone_Object = null;
		$Linkone_Title = trim($this->Linkone_Title);
		if (trim($this->Linkone) != '') {
			switch ($this->Linkone) {
				case 'page':
					if ($this->Linkone_Page > 0 && ($Linkone_Page_c = Page::getByID($this->Linkone_Page)) && !$Linkone_Page_c->error && !$Linkone_Page_c->isInTrash()) {
						$Linkone_Object = $Linkone_Page_c;
						$Linkone_URL = $Linkone_Page_c->getCollectionLink();
						if ($Linkone_Title == '') {
							$Linkone_Title = $Linkone_Page_c->getCollectionName();
						}
					}
					break;
				case 'url':
					$Linkone_URL = $this->Linkone_URL;
					if ($Linkone_Title == '') {
						$Linkone_Title = $Linkone_URL;
					}
					break;
				case 'relative_url':
					$Linkone_URL = $this->Linkone_Relative_URL;
					if ($Linkone_Title == '') {
						$Linkone_Title = $this->Linkone_Relative_URL;
					}
					break;
			}
		}
		$this->set("Linkone_URL", $Linkone_URL);
		$this->set("Linkone_Object", $Linkone_Object);
		$this->set("Linkone_Title", $Linkone_Title);
        $Linktwo_URL = null;
		$Linktwo_Object = null;
		$Linktwo_Title = trim($this->Linktwo_Title);
		if (trim($this->Linktwo) != '') {
			switch ($this->Linktwo) {
				case 'page':
					if ($this->Linktwo_Page > 0 && ($Linktwo_Page_c = Page::getByID($this->Linktwo_Page)) && !$Linktwo_Page_c->error && !$Linktwo_Page_c->isInTrash()) {
						$Linktwo_Object = $Linktwo_Page_c;
						$Linktwo_URL = $Linktwo_Page_c->getCollectionLink();
						if ($Linktwo_Title == '') {
							$Linktwo_Title = $Linktwo_Page_c->getCollectionName();
						}
					}
					break;
				case 'url':
					$Linktwo_URL = $this->Linktwo_URL;
					if ($Linktwo_Title == '') {
						$Linktwo_Title = $Linktwo_URL;
					}
					break;
				case 'relative_url':
					$Linktwo_URL = $this->Linktwo_Relative_URL;
					if ($Linktwo_Title == '') {
						$Linktwo_Title = $this->Linktwo_Relative_URL;
					}
					break;
			}
		}
		$this->set("Linktwo_URL", $Linktwo_URL);
		$this->set("Linktwo_Object", $Linktwo_Object);
		$this->set("Linktwo_Title", $Linktwo_Title);
    }

    public function add()
    {
        $this->addEdit();
    }

    public function edit()
    {
        $this->addEdit();
    }

    protected function addEdit()
    {
        $this->set("Linkone_Options", $this->getSmartLinkTypeOptions([
  'page',
  'url',
  'relative_url',
], true));
        $this->set("Linktwo_Options", $this->getSmartLinkTypeOptions([
  'page',
  'url',
  'relative_url',
], true));
        $this->requireAsset('core/file-manager');
        $this->set('btFieldsRequired', $this->btFieldsRequired);
        $this->set('identifier_getString', Core::make('helper/validation/identifier')->getString(18));
    }

    public function save($args)
    {
        if (isset($args["Linkone"]) && trim($args["Linkone"]) != '') {
			switch ($args["Linkone"]) {
				case 'page':
					$args["Linkone_File"] = '0';
					$args["Linkone_URL"] = '';
					$args["Linkone_Relative_URL"] = '';
					$args["Linkone_Image"] = '0';
					break;
				case 'url':
					$args["Linkone_Page"] = '0';
					$args["Linkone_Relative_URL"] = '';
					$args["Linkone_File"] = '0';
					$args["Linkone_Image"] = '0';
					break;
				case 'relative_url':
					$args["Linkone_Page"] = '0';
					$args["Linkone_URL"] = '';
					$args["Linkone_File"] = '0';
					$args["Linkone_Image"] = '0';
					break;
				default:
					$args["Linkone_Title"] = '';
					$args["Linkone_Page"] = '0';
					$args["Linkone_File"] = '0';
					$args["Linkone_URL"] = '';
					$args["Linkone_Relative_URL"] = '';
					$args["Linkone_Image"] = '0';
					break;	
			}
		}
		else {
			$args["Linkone_Title"] = '';
			$args["Linkone_Page"] = '0';
			$args["Linkone_File"] = '0';
			$args["Linkone_URL"] = '';
			$args["Linkone_Relative_URL"] = '';
			$args["Linkone_Image"] = '0';
		}
        if (isset($args["Linktwo"]) && trim($args["Linktwo"]) != '') {
			switch ($args["Linktwo"]) {
				case 'page':
					$args["Linktwo_File"] = '0';
					$args["Linktwo_URL"] = '';
					$args["Linktwo_Relative_URL"] = '';
					$args["Linktwo_Image"] = '0';
					break;
				case 'url':
					$args["Linktwo_Page"] = '0';
					$args["Linktwo_Relative_URL"] = '';
					$args["Linktwo_File"] = '0';
					$args["Linktwo_Image"] = '0';
					break;
				case 'relative_url':
					$args["Linktwo_Page"] = '0';
					$args["Linktwo_URL"] = '';
					$args["Linktwo_File"] = '0';
					$args["Linktwo_Image"] = '0';
					break;
				default:
					$args["Linktwo_Title"] = '';
					$args["Linktwo_Page"] = '0';
					$args["Linktwo_File"] = '0';
					$args["Linktwo_URL"] = '';
					$args["Linktwo_Relative_URL"] = '';
					$args["Linktwo_Image"] = '0';
					break;	
			}
		}
		else {
			$args["Linktwo_Title"] = '';
			$args["Linktwo_Page"] = '0';
			$args["Linktwo_File"] = '0';
			$args["Linktwo_URL"] = '';
			$args["Linktwo_Relative_URL"] = '';
			$args["Linktwo_Image"] = '0';
		}
        parent::save($args);
    }

    public function validate($args)
    {
        $e = Core::make("helper/validation/error");
        if (in_array("title", $this->btFieldsRequired) && (trim($args["title"]) == "")) {
            $e->add(t("The %s field is required.", t("Title")));
        }
        if (in_array("image", $this->btFieldsRequired) && (trim($args["image"]) == "" || !is_object(File::getByID($args["image"])))) {
            $e->add(t("The %s field is required.", t("Image")));
        }
        if (in_array("description_1", $this->btFieldsRequired) && (trim($args["description_1"]) == "")) {
            $e->add(t("The %s field is required.", t("Description")));
        }
        if ((in_array("Linkone", $this->btFieldsRequired) && (!isset($args["Linkone"]) || trim($args["Linkone"]) == "")) || (isset($args["Linkone"]) && trim($args["Linkone"]) != "" && !array_key_exists($args["Linkone"], $this->getSmartLinkTypeOptions(['page', 'url', 'relative_url'])))) {
			$e->add(t("The %s field has an invalid value.", t("Link 1")));
		} elseif (array_key_exists($args["Linkone"], $this->getSmartLinkTypeOptions(['page', 'url', 'relative_url']))) {
			switch ($args["Linkone"]) {
				case 'page':
					if (!isset($args["Linkone_Page"]) || trim($args["Linkone_Page"]) == "" || $args["Linkone_Page"] == "0" || (($page = Page::getByID($args["Linkone_Page"])) && $page->error !== false)) {
						$e->add(t("The %s field for '%s' is required.", t("Page"), t("Link 1")));
					}
					break;
				case 'url':
					if (!isset($args["Linkone_URL"]) || trim($args["Linkone_URL"]) == "" || !filter_var($args["Linkone_URL"], FILTER_VALIDATE_URL)) {
						$e->add(t("The %s field for '%s' does not have a valid URL.", t("URL"), t("Link 1")));
					}
					break;
				case 'relative_url':
					if (!isset($args["Linkone_Relative_URL"]) || trim($args["Linkone_Relative_URL"]) == "") {
						$e->add(t("The %s field for '%s' is required.", t("Relative URL"), t("Link 1")));
					}
					break;	
			}
		}
        if ((in_array("Linktwo", $this->btFieldsRequired) && (!isset($args["Linktwo"]) || trim($args["Linktwo"]) == "")) || (isset($args["Linktwo"]) && trim($args["Linktwo"]) != "" && !array_key_exists($args["Linktwo"], $this->getSmartLinkTypeOptions(['page', 'url', 'relative_url'])))) {
			$e->add(t("The %s field has an invalid value.", t("Link 2")));
		} elseif (array_key_exists($args["Linktwo"], $this->getSmartLinkTypeOptions(['page', 'url', 'relative_url']))) {
			switch ($args["Linktwo"]) {
				case 'page':
					if (!isset($args["Linktwo_Page"]) || trim($args["Linktwo_Page"]) == "" || $args["Linktwo_Page"] == "0" || (($page = Page::getByID($args["Linktwo_Page"])) && $page->error !== false)) {
						$e->add(t("The %s field for '%s' is required.", t("Page"), t("Link 2")));
					}
					break;
				case 'url':
					if (!isset($args["Linktwo_URL"]) || trim($args["Linktwo_URL"]) == "" || !filter_var($args["Linktwo_URL"], FILTER_VALIDATE_URL)) {
						$e->add(t("The %s field for '%s' does not have a valid URL.", t("URL"), t("Link 2")));
					}
					break;
				case 'relative_url':
					if (!isset($args["Linktwo_Relative_URL"]) || trim($args["Linktwo_Relative_URL"]) == "") {
						$e->add(t("The %s field for '%s' is required.", t("Relative URL"), t("Link 2")));
					}
					break;	
			}
		}
        return $e;
    }

    public function composer()
    {
        $al = AssetList::getInstance();
        $al->register('javascript', 'auto-js-' . $this->btHandle, 'blocks/' . $this->btHandle . '/auto.js', [], $this->pkg);
        $this->requireAsset('javascript', 'auto-js-' . $this->btHandle);
        $this->edit();
    }

    protected function getSmartLinkTypeOptions($include = [], $checkNone = false)
	{
		$options = [
			''             => sprintf("-- %s--", t("None")),
			'page'         => t("Page"),
			'url'          => t("External URL"),
			'relative_url' => t("Relative URL"),
			'file'         => t("File"),
			'image'        => t("Image")
		];
		if ($checkNone) {
            $include = array_merge([''], $include);
        }
		$return = [];
		foreach($include as $v){
		    if(isset($options[$v])){
		        $return[$v] = $options[$v];
		    }
		}
		return $return;
	}
}