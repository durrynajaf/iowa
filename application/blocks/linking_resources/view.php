<?php defined("C5_EXECUTE") or die("Access Denied."); ?>
<div class="lr_box">
<?php if (isset($title) && trim($title) != "") { ?>
    <h3 style="text-align: center;" class="lr_title"><?php echo h($title); ?></h3><?php } ?>
<?php if ($image) { ?>
    <div class="lr_image"><img src="<?php echo $image->getURL(); ?>" alt="<?php echo $image->getTitle(); ?>"/></div><?php } ?>
<?php if (isset($description_1) && trim($description_1) != "") { ?>
    <div class="lr_description"><?php echo $description_1; ?></div><?php } ?>
<div class="lr_buttons">
<?php
if (trim($Linkone_URL) != "") { ?>
    <?php
	$Linkone_Attributes = [];
	$Linkone_Attributes['href'] = $Linkone_URL;
	$Linkone_Attributes['class'] = 'btn btn-default btn-lr';
	$Linkone_AttributesHtml = join(' ', array_map(function ($key) use ($Linkone_Attributes) {
		return $key . '="' . $Linkone_Attributes[$key] . '"';
	}, array_keys($Linkone_Attributes)));
	echo sprintf('<a %s>%s</a>', $Linkone_AttributesHtml, $Linkone_Title); ?><?php
} ?>
<?php
if (trim($Linktwo_URL) != "") { ?>
    <?php
	$Linktwo_Attributes = [];
	$Linktwo_Attributes['href'] = $Linktwo_URL;
	$Linktwo_Attributes['class'] = 'btn btn-default btn-lr';
	$Linktwo_AttributesHtml = join(' ', array_map(function ($key) use ($Linktwo_Attributes) {
		return $key . '="' . $Linktwo_Attributes[$key] . '"';
	}, array_keys($Linktwo_Attributes)));
	echo sprintf('<a %s>%s</a>', $Linktwo_AttributesHtml, $Linktwo_Title); ?><?php
} ?>
</div></div>